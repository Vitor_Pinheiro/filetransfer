#!/bin/bash
#
# /etc/init.d/run-mhub-java.sh

echo "Sync actual date and time..."
service ntp stop
ntpdate us.pool.ntp.org
service ntp start

echo "Date and time sync complete."

echo "Starting mobile hub service..."
java -jar blescan.jar 1 192.168.57.174

exit 0


